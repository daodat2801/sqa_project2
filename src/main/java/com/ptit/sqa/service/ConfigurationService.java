package com.ptit.sqa.service;

import com.ptit.sqa.model.Config;
import com.ptit.sqa.model.Level;
import java.util.List;

public interface ConfigurationService {


    void ConfigInit();
    Config getConfig();
    void setConfig(Config config);
    List<Level> saveConfigToDB(Config config);
    void addLevelToForm();
    void deleteLevelFromForm(Integer idLevel);
}
