package com.ptit.sqa.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
public class ConfigurationServiceTests {
    @Autowired
    ConfigurationService configurationService;
    @Test
    public void testGetConfig(){
    assertNotEquals(null,configurationService.getConfig());
    }
}
